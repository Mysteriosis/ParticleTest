package ch.hes_so.pac.particletest;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.estimote.coresdk.common.requirements.SystemRequirementsChecker;
import com.estimote.coresdk.observation.region.beacon.BeaconRegion;
import com.estimote.coresdk.service.BeaconManager;
import io.particle.android.sdk.cloud.*;
import io.particle.android.sdk.utils.Async;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.UUID;

import static io.particle.android.sdk.utils.Py.list;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_PARTICLE = "PARTICLE";
    private static final int GYRO_EVENT = 1;
    private static final int ACCEL_EVENT = 2;
    private static final int MAGN_EVENT = 3;

    private static final String LOGIN = "pierrealain.curty@master.hes-so.ch";
    private static final String PASSWORD = "pizza32";
    private static final String DEVICE_1 = "b1e249233e6c9a6642066b47"; // IMU Device
    private static final String DEVICE_2 = "b1e249052c96ac22253fbd3b"; // LED Device

    private TextView gyro_x;
    private TextView gyro_y;
    private TextView gyro_z;
    private TextView accel_x;
    private TextView accel_y;
    private TextView accel_z;
    private TextView magn_x;
    private TextView magn_y;
    private TextView magn_z;

    private ToggleButton toggle1;

    private ParticleCloud aCloud;
    private ParticleDevice imuDevice;
    private ParticleDevice ledDevice;
    private Long subscriptionId;

//    private BeaconManager beaconManager;
//    private BeaconRegion region;

    private final Handler subHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            String payloadData = bundle.getString("EventPayload");
            try {
                JSONObject obj = new JSONObject(payloadData);
                switch (msg.what) {
                    case GYRO_EVENT:
                        setGyroValues(obj);
                        break;
                    case ACCEL_EVENT:
                        setAccelValues(obj);
                        break;
                    case MAGN_EVENT:
                        setMagnValues(obj);
                        break;
                }
            } catch (JSONException e) {
                Log.e("JSON", "Couldn't parse event data to JSON");
                e.printStackTrace();
            }
        }
    };

    private final Handler toggleHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            Integer fctResponse = bundle.getInt("FunctionResponse");
            if(fctResponse == 1)
                toggle1.toggle();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParticleCloudSDK.init(MainActivity.this);
        this.aCloud = ParticleCloudSDK.getCloud();

//        this.beaconManager = new BeaconManager(this);
//        this.region = new BeaconRegion("ranged region",
//                UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null);

        this.gyro_x = (TextView) findViewById(R.id.gyro_x);
        this.gyro_y = (TextView) findViewById(R.id.gyro_y);
        this.gyro_z = (TextView) findViewById(R.id.gyro_z);
        this.accel_x = (TextView) findViewById(R.id.accel_x);
        this.accel_y = (TextView) findViewById(R.id.accel_y);
        this.accel_z = (TextView) findViewById(R.id.accel_z);
        this.magn_x = (TextView) findViewById(R.id.magn_x);
        this.magn_y = (TextView) findViewById(R.id.magn_y);
        this.magn_z = (TextView) findViewById(R.id.magn_z);

        this.toggle1 = (ToggleButton) findViewById(R.id.toggle1);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Connect, get and subscribe to device1
        Async.executeAsync(this.aCloud, new Async.ApiWork<ParticleCloud, Long>() {
                    @Override
                    public Long callApi(ParticleCloud particleCloud) throws ParticleCloudException, IOException {

                        Long subID = null;

                try {
                    particleCloud.logIn(LOGIN, PASSWORD);
                } catch (ParticleCloudException e) {
                    Log.e(LOG_PARTICLE, "Couldn't login to cloud");
                    e.printStackTrace();
                }

                try {
                    MainActivity.this.imuDevice = particleCloud.getDevice(DEVICE_1);
                    MainActivity.this.ledDevice = particleCloud.getDevice(DEVICE_2);
                } catch (ParticleCloudException e) {
                    Log.e(LOG_PARTICLE, "Couldn't find Device");
                    e.getStackTrace();
                }

                try {
                    subID = MainActivity.this.imuDevice.subscribeToEvents(
                            "IMU", // Want to catch every events with "IMU" in their name
                            new ParticleEventHandler() {
                                Message msg;
                                public void onEvent(String eventName, ParticleEvent event) {
                                    switch (eventName) {
                                        case "IMU_Gyro":
                                            msg = subHandler.obtainMessage(MainActivity.GYRO_EVENT);
                                            break;
                                        case "IMU_Accel":
                                            msg = subHandler.obtainMessage(MainActivity.ACCEL_EVENT);
                                            break;
                                        case "IMU_Mag":
                                            msg = subHandler.obtainMessage(MainActivity.MAGN_EVENT);
                                            break;
                                    }
                                    Bundle bundle = new Bundle();
                                    bundle.putString("EventPayload", event.dataPayload);
                                    msg.setData(bundle);
                                    subHandler.sendMessage(msg);

                                    Log.i("Subscription event", "Received '" + eventName + "' event with payload: " + event.dataPayload);
                                }

                                public void onEventError(Exception e) {
                                    e.printStackTrace();
                                    Log.e(LOG_PARTICLE, "Event error: ", e);
                                }
                            });
                } catch (IOException e) {
                    Log.e(LOG_PARTICLE, "Couldn't subscribe to events");
                    e.printStackTrace();
                }

                try {
                    int status = MainActivity.this.ledDevice.getIntVariable("ledStatus");
                    Message msg = toggleHandler.obtainMessage(1);
                    Bundle bundle = new Bundle();
                    bundle.putInt("FunctionResponse", status);
                    msg.setData(bundle);
                    toggleHandler.sendMessage(msg);
                } catch (Exception e) {
                    Log.e(LOG_PARTICLE, "Couldn't find ledStatus variable");
                    e.printStackTrace();
                }

                return subID;
            }

            @Override
            public void onSuccess(Long subID) {
                MainActivity.this.subscriptionId = subID;
            }

            @Override
            public void onFailure(ParticleCloudException e) {
                Log.e(LOG_PARTICLE, e.getBestMessage());
                e.printStackTrace();
            }

        });

        // Add Event to toggle button
        toggle1.setOnCheckedChangeListener(null);
        toggle1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Boolean isChecked = toggle1.isChecked();
                final String value = isChecked ? "on" : "off";

                Async.executeAsync(MainActivity.this.ledDevice, new Async.ApiWork<ParticleDevice, Integer>() {
                    @Override
                    public Integer callApi(ParticleDevice particleDevice) throws ParticleCloudException, IOException {
                        Integer resultCode = -1;

                        try {
                            if (particleDevice != null)
                                resultCode = particleDevice.callFunction("toggleLED", list(value));
                        } catch (ParticleDevice.FunctionDoesNotExistException e) {
                            Log.e(LOG_PARTICLE, "Couldn't reach function toggleLED");
                            e.printStackTrace();
                        }

                        return resultCode;
                    }

                    @Override
                    public void onSuccess(Integer res) { }

                    @Override
                    public void onFailure(ParticleCloudException e) {
                        Log.e(LOG_PARTICLE, e.getBestMessage());
                        e.printStackTrace();
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

//        SystemRequirementsChecker.checkWithDefaultDialogs(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            this.imuDevice.unsubscribeFromEvents(this.subscriptionId);
        } catch (ParticleCloudException e) {
            Log.e(LOG_PARTICLE, "Couldn't unsubscribe from cloud");
            e.printStackTrace();
        }
    }

    private void setGyroValues(JSONObject o) throws JSONException {
        this.gyro_x.setText("X: " + o.getString("x"));
        this.gyro_y.setText("Y: " + o.getString("y"));
        this.gyro_z.setText("Z: " + o.getString("z"));
    }

    private void setAccelValues(JSONObject o) throws JSONException {
        this.accel_x.setText("X: " + o.getString("x"));
        this.accel_y.setText("Y: " + o.getString("y"));
        this.accel_z.setText("Z: " + o.getString("z"));
    }

    private void setMagnValues(JSONObject o) throws JSONException {
        this.magn_x.setText("X: " + o.getString("x"));
        this.magn_y.setText("Y: " + o.getString("y"));
        this.magn_z.setText("Z: " + o.getString("z"));
    }
}
